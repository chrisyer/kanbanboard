/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Checklist from './Checklist'
import constants from '../constants'
import {CSSTransition} from 'react-transition-group'
import {DragSource,DropTarget} from 'react-dnd'
import marked from 'marked'
import EditCard from "./EditCard";
import {NavLink} from "react-router-dom";


/**
 * 自定义校验器
 * @param props
 * @param propName
 * @param componentName
 */
let titlePropType = (props, propName, componentName) => {
    if (props[propName]) {
        let value = props[propName]
        if (typeof value !== 'string' || value.length > 20) {
            return new Error(`${propName} in ${componentName} is longer than 20 characters`)
        }
    }
}

const cardDragSpec={
    beginDrag(props){
        return {
            id:props.id,
            status:props.status
        }
    },
    endDrag(props){
        props.cardCallbacks.persistCardDrag(props.id,props.status)
    }
}

const cardDropSpec = {
    hover(props,monitor){
        const  draggedId = monitor.getItem().id
        props.cardCallbacks.updatePosition(draggedId,props.id)
    }
}

let collect = (connect,monitor)=>{
    return {
        connectDragSource:connect.dragSource()
    }
}

let colletDrop = (connect,monitor)=>{
    return {
        connectDropTarget:connect.dropTarget()
    }
}

class Card extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showDetails: false
        }
    }

    handleClick() {
        this.setState({
            showDetails: !this.state.showDetails
        })
    }

    render() {
        let details
        const {connectDragSource,connectDropTarget} = this.props
        if (this.state.showDetails) {
            details =(<div className="card_details">
                    <p dangerouslySetInnerHTML={{__html: marked(this.props.description)}}>
                    </p>
                    <Checklist tasks={this.props.tasks}
                               cardId={this.props.id}
                               taskCallbacks={this.props.taskCallbacks}>
                    </Checklist>
                </div>)

       }
       let sideColor = {
            position: 'absolute',
            zIndex: -1,
            top: 0,
            bottom: 0,
            left: 0,
            width: 7,
            background: this.props.color
        }
        return connectDropTarget(connectDragSource(
            <div className="card">
                <div style={sideColor}/>
                <NavLink to={"/edit/"+this.props.id} className="card_edit">&#9998;</NavLink>
                <div className={this.state.showDetails ? 'card_title card_title_is_open' : 'card_title'}
                     onClick={() => this.handleClick()}>{this.props.title}
                </div>
                <CSSTransition classNames={"toggle"} in={this.state.showDetails}
                               timeout={250} appear={true} enter={true} exit={true}>
                {details===undefined ?
                    <div className="card_details"/>:details}
                </CSSTransition>
            </div>
        ))
    }
}

Card.propTypes = {
    id: PropTypes.number,
    title: titlePropType,
    description: PropTypes.string,
    color: PropTypes.string,
    tasks: PropTypes.arrayOf(PropTypes.object)
}

export default DropTarget(constants.CARD,cardDropSpec,colletDrop)
    (DragSource(constants.CARD,cardDragSpec,collect)(Card))
