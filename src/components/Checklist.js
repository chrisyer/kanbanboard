import React,{Component} from 'react'
import PropTypes from 'prop-types'

class Checklist extends Component{
    checkInputKeyPress(e){
        if(e.key == 'Enter'){
            this.props.taskCallbacks.add(this.props.cardId,e.target.value)
            e.target.value = ''
        }
    }

  render(){
    let tasks = this.props.tasks.map((task,index)=>(
      <li className="checklist_task" key={index}>
        <input type="checkbox" defaultChecked={task.done}
            onChange={this.props.taskCallbacks.toggle.bind(null,this.props.cardId,task.id,index)}/>
          {task.done?<span className="done">{task.name}</span>:<span>{task.name}</span>}
        <a href="#" className="checklist_task_remove"
        onClick={this.props.taskCallbacks.remove.bind(null,this.props.cardId,task.id,index)}/>
      </li>
    ))
    return (
      <div className="checklist">
        <ul>{tasks}</ul>
          <input type="text"
          className="checklist_add_task"
          placeholder="Type then hit Enter to add a task"
          onKeyPress={this.checkInputKeyPress.bind(this)}/>
      </div>
    )
  }
}

Checklist.propTypes = {
  cardId: PropTypes.number.isRequired,
  taskCallbacks: PropTypes.object.isRequired,
  tasks: PropTypes.array.isRequired
}


export  default  Checklist
