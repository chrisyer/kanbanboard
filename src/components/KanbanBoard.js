import React,{Component} from 'react'
import PropTypes from 'prop-types';
import List from './List'
import {DragDropContext} from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import {NavLink, Route, Switch} from "react-router-dom";
import NewCard from "./NewCard";
import EditCard from "./EditCard";

class KanbanBoard extends Component{
  render(){
    return (
      <div className="app">
          <NavLink to='/new' className="float-button">+</NavLink>
            <List id="todo" title="To Do"
                  taskCallbacks={this.props.taskCallbacks}
                  cardCallbacks={this.props.cardCallbacks}
                  cards={ this.props.cards.filter((card)=>card.status === 'todo')}
            />
            <List id="in-progress" title="In Progress"
                  taskCallbacks={this.props.taskCallbacks}
                  cardCallbacks={this.props.cardCallbacks}
                  cards={ this.props.cards.filter((card)=>card.status === 'in-progress')}
            />
            <List id="done" title="Done"
                  taskCallbacks={this.props.taskCallbacks}
                  cardCallbacks={this.props.cardCallbacks}
                  cards={ this.props.cards.filter((card)=>card.status === 'done')}
            />
            <Switch>
                <Route path="/new" render={(props)=>(<NewCard cardCallbacks={this.props.cardCallbacks} {...props}/>)}/>
                <Route path="/edit/:card_id" render={(props)=>(<EditCard cards={this.props.cards}
                                                                         cardCallbacks={this.props.cardCallbacks}
                                                                         {...props}/>)}/>
            </Switch>
      </div>
    )
  }

}
KanbanBoard.propTypes = {
  cardCallbacks: PropTypes.object.isRequired,
  cards: PropTypes.array.isRequired,
  taskCallbacks: PropTypes.object.isRequired
}
export default DragDropContext(HTML5Backend)(KanbanBoard)
