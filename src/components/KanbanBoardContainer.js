
/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, {Component} from 'react'
import {throttle} from '../utils'
import KanbanBoard from './KanbanBoard'
import 'whatwg-fetch'
import update from 'immutability-helper'
import {BrowserRouter, withRouter} from "react-router-dom";


const API_URL = 'http://kanbanapi.pro-react.com'
const API_HEADER = {
    'Content-Type':'application/json',
    'Authorization':'saxsjknxcsk'
}

class KanbanBoardContainer extends Component {

    constructor(props){
        super(props)
        this.state = {
            cards:[]
        }
        this.updateCardStatus = throttle(this.updateCardStatus.bind(this))
        this.updateCardPosition = throttle(this.updateCardPosition.bind(this),500)
    }

    persistCardDrag(cardId,status){
        let cardIndex = this.state.cards.findIndex((card)=>card.id===cardId)
        let card = this.state.cards[cardIndex]
        fetch(`${API_URL}/cards/${cardId}`,{
            method:'put',
            headers:API_HEADER,
            body:JSON.stringify({
                status:card.status,
                row_order_position:cardIndex
            })}).then(response=>{
                if(!response.ok){
                    return new Error('Server response wasn\'t OK')
                }
                return response
            }).catch(error=>{
                console.log('Fetch Error: ',error)
                this.setState(update(this.state,{
                    cards:{
                        [cardIndex]:{
                            status:{
                                $set:status
                            }
                        }
                    }
                }))
            })
    }


    componentDidMount() {
        fetch(API_URL + '/cards',
            {headers: API_HEADER})
            .then(response => response.json())
            .then(data => this.setState({
                cards: data
            }))
            .catch(error => {
                console.log('Error fetching and parsing data', error)
            })
    }

    addTask(cardId,taskName){
        let cardIndex = this.state.cards.findIndex((card)=>card.id===cardId)
        let newTask = {id:Date.now(),name:taskName,done:false}
        let newState = update(this.state.cards,{
            [cardIndex]:{
                tasks:{
                    $push:[newTask]
                }
            }
        })
        let prevState = this.state
        fetch(`${API_URL}/cards/${cardId}/tasks/`,{
            method:'post',
            headers:API_HEADER,
            body:JSON.stringify(newTask)
        }).then(response=>{
                if(!response.ok){
                    return new Error('Server response wasn\'t ok')
                }
                return response.json()
            })
            .then(data=>{
                newTask.id = data.id
                this.setState({
                    cards:newState
                })
            })
            .catch(error=>{
                console.error('Fetch Error: ',error)
                this.setState(prevState)
            })
    }


    removeTask(cardId,taskId,taskIndex){
        let cardIndex = this.state.cards.findIndex((card)=>card.id===cardId)
        let newState = update(this.state.cards,{
            [cardIndex]:{
                tasks:{$splice:[[taskIndex,1]]}
            }
        })
        this.setState({cards:newState})
        fetch(`${API_URL}/cards/${cardId}/tasks/${taskId}`,{
            method:'delete',
            headers:API_HEADER
        })
    }
    toggleTask(cardId,taskId,taskIndex){
        let cardIndex = this.state.cards.findIndex((card)=>card.id===cardId)
        let newDoneValue = false
        let newState = update(this.state.cards,{
            [cardIndex]:{
                tasks:{
                    [taskIndex]:{
                        done:{
                            $apply:(done)=>{
                                newDoneValue = !done
                                return newDoneValue
                            }
                        }
                    }
                }
            }
        })
        this.setState({cards:newState})
        fetch(`${API_URL}/cards/${cardId}/tasks/${taskId}`,{
            method:'put',
            headers:API_HEADER,
            body:JSON.stringify({done:newDoneValue})
        })
    }

    /**
     * 更新卡片的状态
     * @param cardId
     * @param listId
     */
    updateCardStatus(cardId,status){
        let cardIndex = this.state.cards.findIndex((card)=>card.id===cardId)
        let card = this.state.cards[cardIndex]
        if(card.status !== status){
            this.setState(update(this.state,{
                cards:{
                    [cardIndex]:{
                        status:{$set:status}
                    }
                }
            }))
        }
    }

    updateCardPosition(cardId,afterId){
        if(cardId !== afterId){
            let cardIndex = this.state.cards.findIndex((card)=>card.id===cardId)
            let card = this.state.cards[cardIndex]
            let afterIndex = this.state.cards.findIndex((card)=>card.id===afterId)
            this.setState(update(this.state,{
                cards:{
                    $splice:[
                        [cardIndex,1],
                        [afterIndex,0,card]
                    ]
                }
            }))
        }
    }

    addCard(card){
        let prevState = this.state
        if(card.id === null){
            card = {
                ...card,
                id:Date.now()
            }
        }
        let nextState = update(this.state.cards,{
            $push:[card]
        })
        this.setState({cards:nextState})
        //fetch
        fetch(`${API_URL}/cards`,{
            headers:API_HEADER,
            method:'post',
            body:JSON.stringify(card)
        })
            .then((response)=>{
                if(response.ok){
                    return response.json()
                }else {
                    throw  new Error("Server response wasn't ok")
                }
            })
            .then((data)=>{
                card.id = data.id
                this.setState({
                    cards:nextState
                })
            })
            .catch(err=>{
                this.setState(prevState)
            })

    }

    updateCard(card){
        let prevState = this.state
        let cardIndex = this.state.cards.findIndex((c)=>c.id===card.id)
        let nextState = update(this.state.cards,{
            [cardIndex]:{
                $set:card
            }
        })
        this.setState({cards:nextState})
        //fetch
        fetch(`${API_URL}/cards/${card.id}`,{
            method:'put',
            headers:API_HEADER,
            body:JSON.stringify(card)
        })
            .then((response)=>{
                if(!response.ok){
                    throw  new Error("Server response wasn't ok")
                }
            })
            .catch(err=>{
                this.setState(prevState)
            })
    }

    render() {
        return (
            <BrowserRouter>
                <KanbanBoard cards={this.state.cards}
                    taskCallbacks={{
                        toggle:this.toggleTask.bind(this),
                        add:this.addTask.bind(this),
                        remove:this.removeTask.bind(this)
                    }}
                    cardCallbacks={{
                        updateStatus:this.updateCardStatus,
                        updatePosition:this.updateCardPosition,
                        persistCardDrag: this.persistCardDrag.bind(this),
                        updateCard:this.updateCard.bind(this),
                        addCard:this.addCard.bind(this)
                    }}/>
            </BrowserRouter>
        )
    }
}

export default KanbanBoardContainer