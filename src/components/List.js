import React,{Component} from 'react'
import PropTypes from 'prop-types'
import Card from './Card'
import {DropTarget} from 'react-dnd'
import constants from '../constants'

const listSpec = {
    hover(props,monitor){
        const draggedId = monitor.getItem().id
        props.cardCallbacks.updateStatus(draggedId,props.id)
    }
}
let collet = (connect,monitor)=>{
    return {
        connectDropTarget:connect.dropTarget()
    }
}
class List extends Component{
  render(){
      const {connectDropTarget} = this.props
    let cards = this.props.cards.map((card)=>(
      <Card key={card.id}
            {...card}
            cardCallbacks={this.props.cardCallbacks}
            taskCallbacks={this.props.taskCallbacks}
      />
    ))
    return connectDropTarget(
      <div className="list">
        <h1>{this.props.title}</h1>
        {cards}
      </div>
    )
  }
}

List.propTypes = {
    title:PropTypes.string.isRequired,
    cards:PropTypes.arrayOf(PropTypes.object),
    taskCallbacks:PropTypes.object.isRequired,
    cardCallbacks:PropTypes.object.isRequired
}

export default DropTarget(constants.CARD,listSpec,collet)(List)
