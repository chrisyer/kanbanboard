import React from 'react'
import ReactDOM from 'react-dom'
import './styles/app.css'
import KanbanBoardContainer from './components/KanbanBoardContainer'



// Render the main component into the dom
ReactDOM.render(<KanbanBoardContainer />, document.getElementById('root'));

