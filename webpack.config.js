const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry:[
        './src/index.js',
    ],
    output:{
        path:path.join(__dirname, 'dist'),
        filename:'bundle.js',
        //publicPath:'/assets/'
    },
    module:{
        loaders:[{
            test:/\.js[x]?$/,
            loader:'babel-loader',
            exclude: /node_modules/
        },{
            test: /\.css$/,
            use: [ 'style-loader', 'css-loader' ]
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template:'src/templates/index.html'
        })
    ]

}